<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Link;
use App\LinkDevice;
use App\Shortener;
use App\Matcher;

class LinksController extends Controller
{
    public function __construct() {
        $this->shortener = new Shortener();
    }

    public function postSubmit(Request $request) {
        $this->validate($request, [
            'url' => 'required',
            'url.desktop' => 'sometimes|url',
            'url.tablet'  => 'sometimes|url',
            'url.mobile'  => 'sometimes|url',
        ]);

        $url = $request->input('url');
        $devices = [];
        if (is_array($url)) {
            foreach ($url as $type => $url) {
                $devices[] = new LinkDevice([
                    'url' => $url,
                    'type' => $type,
                ]);
            }
        } else {
            $devices[] = new LinkDevice([
                'url' => $url,
                'type' => 'desktop',
            ]);
        }

        $link = Link::create();
        $link->devices()->saveMany($devices);

        $short_url = $this->shortener->getShortUrl($link);

        return [
            'link' => $link->fresh(),
            'short_url' => $short_url,
        ];
    }

    public function getRedirect(Request $request, $short_id) {
        $link = $this->shortener->loadLinkFromShort($short_id);
        $matcher = new Matcher($link);

        $redirect_device = $matcher->matchRedirectDevice();
        $redirect_device->incrementRedirect();

        return redirect()->to($redirect_device->url);
    }

    public function getList(Request $request) {
        $links = Link::all();

        return $links;
    }
}
