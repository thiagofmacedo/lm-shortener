<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});


$app->post('/links/submit', 'LinksController@postSubmit');
$app->get('/links/list', 'LinksController@getList');

$app->get('/{short_id}', 'LinksController@getRedirect');
