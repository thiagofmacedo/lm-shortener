<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkDevice extends Model
{
    public $timestamps = false;
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url', 'type', 'redirects'
    ];
    protected $hidden = ['link_id'];


    public function incrementRedirect() {
        $DB = app('db');
        $device = $this;

        $DB->transaction(function() use ($device, $DB) {
            $DB->table($device->getTable())
                ->where('link_id', $device->link_id)
                ->where('type', $device->type)
                ->increment('redirects');
        }, 20);
    }
}
