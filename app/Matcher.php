<?php 
namespace App;

use App\Link;

class Matcher
{
    function __construct($link) {
        $this->link = $link;
        $this->detector = app('Mobile_Detect');
    }

    function matchRedirectDevice() {
        if ($this->isMobile()) {
            return $this->link->getDevice('mobile');
        }
        elseif ($this->isTablet()) {
            return $this->link->getDevice('tablet');
        }
        elseif ($this->isDesktop()) {
            return $this->link->getDevice('desktop');
        }
    }

    function isDesktop() {
        return (!$this->isMobile() && !$this->isTablet()
                && !empty($this->link->getDevice('desktop')));
    }

    function isTablet() {
        return ($this->detector->isTablet()
                && !empty($this->link->getDevice('tablet')));
    }

    function isMobile() {
        return ($this->detector->isMobile() && !$this->detector->isTablet()
                && !empty($this->link->getDevice('mobile')));
    }
}