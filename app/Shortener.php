<?php 
namespace App;

use App\Link;
use App\LinkDevice;

class Shortener
{
    function getShortId(Link $link) {
        return strtr(rtrim(base64_encode(pack('i', $link->id)), '='), '+/', '-_');
    }

    function getLinkFromShort($short) {
        $number = unpack('i', base64_decode(str_pad(strtr($short, '-_', '+/'),
                            strlen($short) % 4, '=')));
        return $number[1];
    }

    function loadLinkFromShort($short) {
        $id = $this->getLinkFromShort($short);
        return Link::find($id);
    }

    function getShortUrl(Link $link) {
        $code = $this->getShortId($link);
        $base_url = env('SHORTNER_URL', array_get($_SERVER, 'HTTP_HOST', ''));

        return $base_url ."/". $code;
    }
}