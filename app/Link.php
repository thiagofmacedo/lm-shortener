<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $appends = ['redirects'];
    protected $with = ['devices'];


    public function devices() {
        return $this->hasMany('App\LinkDevice');
    }

    public function getDevice($device_type) {
        return $this->devices->where('type', $device_type)->first();
    }


    public function getShortUrlAttribute() {
        $link = $this;
        return with(new Shortener())
                    ->getShortUrl($link);
    }

    public function getShortIdAttribute() {
        $link = $this;
        return with(new Shortener())
                    ->getShortId($link);
    }

    public function getRedirectsAttribute() {
        if (!$this->isDirty() && $this->relationLoaded('devices')) {
            return $this->devices->sum('redirects');
        } else {
            return $this->devices()->sum('redirects');
        }
    }
}
