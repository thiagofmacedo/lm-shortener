<?php

use App\Link;
use App\LinkDevice;

class LinkControllerTest extends TestCase
{
    public function testLinkSubmit()
    {
        $link = factory(LinkDevice::class)->make();

        $data = ['url' => $link->url];
        $this->post('/links/submit', $data)
            ->seeStatusCode(200)
            ->seeJsonStructure(['short_url', 'link' => ['devices']]);

        $response = $this->response->original;
        $this->assertEquals($link->url, $response['link']->getDevice('desktop')->url);
    }

    public function testLinkSubmitDevices()
    {
        $devices = factory(LinkDevice::class, 3)->make();
        
        $data = ['url' => []];
        foreach ($devices as $device) {
            $data['url'][$device->type] = $device->url; 
        }

        $this->post('/links/submit', $data)
            ->seeStatusCode(200)
            ->seeJsonStructure(['short_url', 'link' => ['devices']]);

        $response = $this->response->original;
        $this->assertEquals($devices->toArray(), $response['link']->devices->toArray());
    }

    public function testLinkRedirect() {
        $link = $this->createLinkWithDevices();

        $this->get('/'. $link->short_id)
            ->seeStatusCode(302)
            ->seeHeader('location');

        $location = $this->response->headers->get('location');
        $this->assertEquals($location, $link->getDevice('desktop')->url);

        $link = $link->fresh();
        $this->assertEquals(1, $link->redirects);
    }

    public function testLinkList() {
        $link = $this->createLinkWithDevices();

        $this->get('/links/list')
            ->seeStatusCode(200)
            ->seeJsonStructure(['*' => ['redirects', 'devices' => 'redirects']]);

        $response = json_decode($this->response->getContent(), true);
        $this->assertEquals($response, [$link->fresh()->toArray()]);
    }
}
