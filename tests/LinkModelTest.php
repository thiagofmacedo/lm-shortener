<?php

use App\Link;
use App\LinkDevice;

class LinkModelTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreation()
    {
        $devices = factory(LinkDevice::class, 2)->make();
        $link = factory(Link::class)->create();
        $link->devices()->saveMany($devices);

        $this->seeInDatabase('links', [
            'id' => $link->id,
        ]);
        $this->seeInDatabase('link_devices', [
            'url' => $devices[0]->url,
        ]);
    }

    public function testLinkDevice() {
        $link = $this->createLinkWithDevices();

        $this->assertContainsOnlyInstancesOf(LinkDevice::class, $link->devices);
        $this->assertInstanceOf(LinkDevice::class, $link->getDevice('desktop'));
    }

    public function testShortUrl() {
        $link = factory(Link::class)->create();

        $this->assertNotEmpty($link->short_url);
    }

    public function testShortId() {
        $link = factory(Link::class)->create();

        $this->assertNotEmpty($link->short_id);
    }

    public function testDeviceIncrementRedirects() {
        $link = $this->createLinkWithDevices();
        $device = $link->getDevice('desktop');

        $this->assertEquals(0, $device->redirects);

        $device->incrementRedirect();
        $link = $link->fresh();
        $this->assertEquals(1, $link->redirects);

        $device->incrementRedirect();
        $link = $link->fresh();
        $this->assertEquals(2, $link->redirects);
    }
}
