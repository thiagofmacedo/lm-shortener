<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class TestCase extends Laravel\Lumen\Testing\TestCase
{
    use DatabaseTransactions;
    
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    public function createLinkWithDevices() {
        $devices = factory(App\LinkDevice::class, 3)->make();
        $link = factory(App\Link::class)->create();
        $link->devices()->saveMany($devices);

        return $link;
    }
}
