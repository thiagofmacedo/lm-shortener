<?php

use App\Link;
use App\Matcher;

class MatcherRepositoryTest extends TestCase
{
    public function testmatchRedirectDevice()
    {
        $link = $this->createLinkWithDevices();
        $matcher = new Matcher($link);

        $redirect_device = $matcher->matchRedirectDevice();
        $this->assertEquals($link->getDevice('desktop')->url, $redirect_device->url);
    }

    public function testMatchMobileRedirect() {
        app()->singleton('Mobile_Detect', function(){ return new Mobile_Detect(); });
        $detector = app('Mobile_Detect');

        $detector->setUserAgent('Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0');

        $link = $this->createLinkWithDevices();
        $matcher = new Matcher($link);

        $redirect_device = $matcher->matchRedirectDevice();
        $this->assertEquals($link->getDevice('mobile')->url, $redirect_device->url);
    }

    public function testMatchTabletRedirect() {
        app()->singleton('Mobile_Detect', function(){ return new Mobile_Detect(); });
        $detector = app('Mobile_Detect');
        
        $detector->setUserAgent('Mozilla/5.0 (Android 4.4; Tablet; rv:41.0) Gecko/41.0 Firefox/41.0');

        $link = $this->createLinkWithDevices();
        $matcher = new Matcher($link);

        $redirect_device = $matcher->matchRedirectDevice();
        $this->assertEquals($link->getDevice('tablet')->url, $redirect_device->url);
    }
}
