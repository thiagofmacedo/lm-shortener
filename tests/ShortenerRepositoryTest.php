<?php

use App\Link;
use App\Shortener;

class ShortenerRepositoryTest extends TestCase
{
    function setUp() {
        parent::setUp();

        $this->shortener = new Shortener();
    }

    public function testGetShortId()
    {
        $link = factory(Link::class)->create();
        $short = $this->shortener->getShortId($link);

        $this->assertNotEmpty($short);
    }

    public function testGetLinkFromShort() {
        $link = factory(Link::class)->create();
        $short_id = $this->shortener->getShortId($link);
        $link_id = $this->shortener->getLinkFromShort($short_id);

        $this->assertEquals($link->id, $link_id);
    }

    public function testLoadLinkFromShort() {
        $link = factory(Link::class)->create();
        $short_id = $this->shortener->getShortId($link);
        
        $short_link = $this->shortener->loadLinkFromShort($short_id);

        $this->assertInstanceOf(Link::class, $short_link);
        $link = $link->fresh();
        $this->assertEquals($link->toArray(), $short_link->toArray());
    }

    public function testGetShortUrl() {
        $link = factory(Link::class)->create();
        $short_url = $this->shortener->getShortUrl($link);

        $this->assertNotEmpty($short_url);
        $this->assertContains($this->shortener->getShortId($link), $short_url);
    }
}
