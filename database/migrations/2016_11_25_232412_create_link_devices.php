<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkDevices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_devices', function($t)
        {
            $t->integer('link_id');
            $t->foreign('link_id')
                ->references('id')
                ->on('links');

            $t->string('url');
            $t->enum('type', ['desktop', 'tablet', 'mobile']);
            $t->integer('redirects')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('link_devices');
    }
}
