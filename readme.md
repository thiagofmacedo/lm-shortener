# Laravel-PHP Url Shortener

### Installation

    composer install
    php artisan migrate

### Testing

    phpunit

The project tests are in `tests` folder.

### Running

    php -S localhost:8000 -t ./public

The configured routes are:

    POST /links/submit
            {url: {desktop: '', tablet:'', mobile: ''}}
    GET  /links/list
    GET  /{short_link}

